from django.test import TestCase, Client
from django.urls import resolve
from homepage.views import home

# Create your tests here.
class UnitTestStory6(TestCase):
	def test_homepage_url_is_exist(self):
		response = Client().get('/home/')
		self.assertEqual(response.status_code, 200)

	def test_notexist_url_is_notexist(self):
		response = Client().get('/gaada/')
		self.assertEqual(response.status_code, 404)
